<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('towns', function (Blueprint $table) {
//            $table->foreign('mayor_id')->references('id')->on('mayors');
//            $table->foreign('town_hall_id')->references('id')->on('town_halls');
//        });
        Schema::table('mayors', function (Blueprint $table) {
            $table->foreign('town_id')->references('id')->on('towns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('towns', function (Blueprint $table) {
//            $table->dropForeign('towns_mayor_id_foreign');
//            $table->dropForeign('towns_town_hall_id_foreign');
//        });
        Schema::table('mayors', function (Blueprint $table) {
            $table->dropForeign('mayors_town_id_foreign');
        });
    }
}
