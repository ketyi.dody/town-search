<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coordinates';

    /**
     * @var string
     */
    protected $lat;

    /**
     * @var string
     */
    protected $alt;
}
