<?php
namespace App\Repositories;

use App\Town;
use Illuminate\Support\Facades\DB;

class TownRepository
{

    /**
     * Get's a post by it's ID
     *
     * @param int
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */
    public function get($townId)
    {
        return DB::table('towns')
            ->join('mayors', 'towns.id', '=', 'mayors.town_id')
            ->select(
                [
                    'towns.id as townId',
                    'towns.name as townName',
                    'towns.phone as townPhone',
                    'towns.email as townEmail',
                    'towns.web as townWeb',
                    'towns.erb as townErb',
                    'towns.type as townType',
                    'mayors.id as mayorId',
                    'mayors.name as mayorName',
                    'mayors.phone as mayorPhone',
                ]
            )
            ->where('towns.id', $townId)
            ->first()
        ;
    }

    /**
     * Get's all posts.
     *
     * @return mixed
     */
    public function all()
    {
        return Town::all();
    }

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($townId)
    {
        Town::destroy($townId);
    }

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($townId, array $townData)
    {
        Town::find($townId)->update($townData);
    }

    /**
     * @param $searchTerm
     * @return \Illuminate\Support\Collection
     */
    public function findByTownOrMayorName($searchTerm)
    {
        return DB::table('towns')
            ->join('mayors', 'towns.id', '=', 'mayors.town_id')
            ->select(
                [
                    'towns.id as value',
                    'towns.name as label',
                ]
            )
            ->where('towns.name', 'like', '%' . $searchTerm . '%')
            ->orWhere('mayors.name', 'like', '%' . $searchTerm . '%')
            ->get()
        ;
    }
}
