<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TownHall extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'town_halls';

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $zip;

    /**
     * @var string
     */
    protected $country;

    /**
     * Get the coordinates record associated with the town hall.
     */
    public function coordinate()
    {
        return $this->hasOne('App\Coordinates');
    }

    public function town() {
        $this->belongsTo('App\Town');
    }
}
