<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mayor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mayors';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    public function town() {
        $this->belongsTo('App\Town', 'town_id', 'id');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
}
