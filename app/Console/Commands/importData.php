<?php

namespace App\Console\Commands;

use App\Services\TownImportService;
use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;

class importData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    protected $url = 'https://www.e-obce.sk/zoznam_vsetkych_obci.html';

    protected $townImportService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Data from www.e-obce.sk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TownImportService $townImportService)
    {
        parent::__construct();
        $this->townImportService = $townImportService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $index = 0;
        $pages = $this->townImportService->getTownPages();

        foreach ($pages as $page) {
            $townsPage = HtmlDomParser::file_get_html($page, false, null, 0);
            foreach ($townsPage->find('a') as $element) {
                if (strpos($element->href, 'https://www.e-obce.sk/obec') !== false) {
                    $index++;
                    $townName = $this->townImportService->getTown($element->href, $index);
                    if ($townName) {
                        $this->info($index . ' - ' . $townName . ' Imported');
                    } else {
                        $this->error($element->href . ' is not working, skipping..');
                    }
                }
            }
        }
    }
}
