<?php

namespace App\Services;

use App\Mayor;
use App\Town;
use Sunra\PhpSimple\HtmlDomParser;

class TownImportService
{
    public function getTownPages()
    {
        $pages = [];
        $dom = HtmlDomParser::file_get_html( 'https://www.e-obce.sk/zoznam_vsetkych_obci.html', false, null, 0 );
        foreach ($dom->find('a') as $element) {
            if (strpos($element->href,  'https://www.e-obce.sk/zoznam_vsetkych_obci.html?strana=') !== false)
                $pages[] = $element->href;
        }

        return $pages;
    }

    public function getTown($url, $index, $exception = false)
    {
        $town = new Town();
        $mayor = new Mayor();
        try {
            $dom = HtmlDomParser::file_get_html( $url, false, null, 0 );
        } catch (\Exception $exception) {
            if (!$exception) {
                $this->getTown($url, $index, true);
            }
            return false;
        }
        $town = $this->setTownNameAndType($dom, $town);

        $this->setTownAttributes($dom, $town, $mayor);

        return $town->name;
    }

    protected function setTownNameAndType($dom, $town)
    {
        foreach($dom->find('h1') as $element) {
            $town->type = '';
            switch($element->text()) {
                case strpos('Obec ', $element->text()) == 0 :
                    $townName = str_replace('Obec ', '', $element->text());
                    $town->type = 'town';
                    break;
                case strpos('Mesto ', $element->text()) == 0 :
                    $townName = str_replace('Mesto ', '', $element->text());
                    $town->type = 'city';
                    break;
                case strpos('Vojenský obvod ', $element->text()) == 0 :
                    $townName = str_replace('Vojenský obvod ', '', $element->text());
                    $town->type = 'military_district';
                    break;
                default :
                    $townName = 'N/A';
                    $town->type = '';
                    break;
            }

            $town->name = $townName;
        }

        return $town;
    }

    protected function setTownAttributes($dom, $town, $mayor)
    {
        foreach($dom->find('img') as $element) {
            if (strpos($element->title, 'Erb') !== false) {
                $town->erb = $element->src;
            }
        }

        foreach($dom->find('td') as $element) {
            switch (str_replace(' ', '', $element->plaintext)) {
                case 'Tel:' : $town->phone = str_replace(' ', '',$element->nextSibling()->plaintext); break;
                case 'Email:' : $town->email = str_replace(' ', '',$element->nextSibling()->plaintext); break;
                case 'Web:' : $town->web = str_replace(' ', '',$element->nextSibling()->plaintext); break;
                case 'Primátor:' :
                case 'Starosta:' :
                    $mayor->name = $element->nextSibling()->plaintext; break;
                case 'Mobil:' : $mayor->phone = str_replace(' ', '',$element->nextSibling()->plaintext); break;
            }
        }

        $town->save();

        $town->mayor()->save($mayor);
    }
}
