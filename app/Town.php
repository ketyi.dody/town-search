<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'towns';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $web;

    /**
     * @var string
     */
    protected $erb;

    /**
     * @var string
     */
    protected $type;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getWeb(): string
    {
        return $this->web;
    }

    /**
     * @param string $web
     */
    public function setWeb(string $web): void
    {
        $this->web = $web;
    }

    /**
     * @return string
     */
    public function getErb(): string
    {
        return $this->erb;
    }

    /**
     * @param string $erb
     */
    public function setErb(string $erb): void
    {
        $this->erb = $erb;
    }

    /**
     * Get the mayor record associated with the town.
     */
    public function mayor()
    {
        return $this->hasOne('App\Mayor');
    }

    /**
     * Get the town hall record associated with the town.
     */
    public function townHall()
    {
        return $this->hasOne('App\TownHall');
    }
}
