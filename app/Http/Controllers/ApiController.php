<?php

namespace App\Http\Controllers;

use App\Repositories\TownRepository;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @var TownRepository
     */
    protected $townRepository;

    /**
     * PostController constructor.
     *
     * @param TownRepository $townRepository
     */
    public function __construct(TownRepository $townRepository)
    {
        $this->townRepository = $townRepository;
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('term');
        $results = $this->townRepository->findByTownOrMayorName($searchTerm);

        return response()->json($results);
    }
}
