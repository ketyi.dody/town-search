<?php

namespace App\Http\Controllers;

use App\Repositories\TownRepository;

class SearchController extends Controller
{
    /**
     * @var TownRepository
     */
    protected $townRepository;

    /**
     * PostController constructor.
     *
     * @param TownRepository $townRepository
     */
    public function __construct(TownRepository $townRepository)
    {
        $this->townRepository = $townRepository;
    }

    public function index()
    {
        return view('search.index');
    }
}
