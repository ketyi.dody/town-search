<?php

namespace App\Http\Controllers;

use App\Repositories\TownRepository;
use App\Town;

class townController extends Controller
{
    /**
     * @var TownRepository
     */
    protected $townRepository;

    /**
     * PostController constructor.
     *
     * @param TownRepository $townRepository
     */
    public function __construct(TownRepository $townRepository)
    {
        $this->townRepository = $townRepository;
    }

    public function show($town_id)
    {
        $town = $this->townRepository->get($town_id);
        return view('town.show', compact(['town']));
    }
}
