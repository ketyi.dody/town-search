# Town search

This is a project for UI42.

## Installation
Clone the repository
```bash
git clone https://gitlab.com/ketyi.dody/town-search.git
```
Cd into the directory
```bash
cd town-search
```
Install composer dependencies
```bash
php composer install
```
Install NPM dependencies
```bash
npm run develeopment
```
To get NPM go to [npm install page](https://www.npmjs.com/get-npm).

In your mysql creat the database.
```mysql
CREATE DATABASE town_search;
```
Create database table structure
```bash
php artisan migrate
```
Import data
```bash
php artisan data:import
```
Start the server
```bash
php artisan serve
```

# What has been done
\- Database structure for the basic application

\- Data migration

\- basic layout for search and town detail (using bootstrap)

\- Town search based on town or mayor name

\- Search autocomplete  

# What is missing
\- Fulltext search (regarding my research Laravel scout or elastic search could be used here)

\- Complete layout with links in footer and fine-tune components 

\- GPS coordinates search for town halls

\- Gathering additional import data from source

\- Town "Erb" download

\- Using Queues for data import

\- Optimize data import for possible duplicates (this part haven't been implemented)

# Conclusion
The data import service is not the nicest code, unfortunately the creator of the web page [e-obce.sk](wwww.e-obce.sk) didn't use any kinds of marking on the fields like id="town_name". That would be than much easier to parse the data.

I've not imported a lot of data from the page in the rush to provide with some "example code" to you, instead of completing the whole web page, I apologize for that, but I had some other work to be done as well, and the weekend is only 48h long.

The frontend might not be the same as in the attached picture, but the reason is the same as with the other parts of the application, not enough time.

Also please bear in mind that Laravel is a completely new framework for me to work with, so I had to learn from the basics as working on this project.

Overall work time: 19h   
