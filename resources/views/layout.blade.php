<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo_small.png') }}"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>Town Search</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <header>
            @include('partials.header')
        </header>
    </div>
    @yield('content')
    <div class="row">
        <footer>
            @include('partials.footer')
        </footer>
    </div>
</div>
</body>
</html>
