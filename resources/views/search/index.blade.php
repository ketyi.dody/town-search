@extends('layout')

@section('content')
    <div class="row searchField">
        <div class="col-lg-10 offset-1 text-center align-content-center align-middle">

            <div class="search-title">
                <h2>
                    Vyhladavat v databaze obci
                </h2>
            </div>
            <div class="search-box">
                <form method="GET" action="/" name="search">
                    @csrf
                    <input id="town-search" class="form-control input-group" name="term" for="search"
                           placeholder="Zadajte nazov"/>
                </form>
            </div>
        </div>
    </div>

@endsection
