@extends('layout')

@section('content')
    {{ $term }} :
    <ul>
        @foreach($results as $result)
            @if ($result)
                <li> <a href="/town/{{ $result->townId }}">{{ $result->townName }}</a> </li>
            @endif
        @endforeach
    </ul>
@endsection
