<div class="row">
    <div class="col-lg-2 offset-1">
        <a href="/"><img src="{{ asset('img/logo_big.png') }}" alt="logo"/></a>
    </div>
    <div class="col-lg-2 offset-3">
        <a href="#">Kontakty a cisla na oddelenia</a>
    </div>
    <div class="col-lg-4">
        <select class="browser-default custom-select">
            <option value="en">EN</option>
            <option value="sk">SK</option>
            <option value="cz">CZ</option>
        </select>
        <input type="text" placeholder="Vyhladat" />
        <input class="btn btn-success" type="button" value="Prihlasenie" />
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="offset-1 col-lg-10">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item"><a href="#">O nas</a></li>
            <li class="list-group-item"><a href="#">Zoznam miest</a></li>
            <li class="list-group-item"><a href="#">Inspekcia</a></li>
            <li class="list-group-item"><a href="#">Kontakt</a></li>
        </ul>
    </div>
</div>
