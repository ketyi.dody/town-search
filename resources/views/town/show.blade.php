@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-4 offset-4 text-center align-middle align-items-center town-title">
            Detail Obce
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 offset-1 town-card">
            <div class="row">
                <div class="col-lg-6 town-detail">
                    <div class="row">
                        <div class="col-lg-5 offset-1 town-detail-labels">
                            <p>Meno starostu:</p>
                            <p>Adresa obecneho uradu:</p>
                            <p>Telefon:</p>
                            <p>Email:</p>
                            <p>Web:</p>
                            <p>Zemepisne suradnice</p>
                        </div>
                        <div class="col-lg-6 town-detail-data">
                            <p>{{ $town->mayorName }}</p>
                            <p>N/A</p>
                            <p>{{ $town->townPhone }}</p>
                            <p>{{ $town->townEmail }}</p>
                            <p>{{ $town->townWeb }}</p>
                            <p>N/A</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 align-items-center text-center town-erb">
                    <img src="{{ $town->townErb }}" alt="Erb_{{ $town->townName }}"/><br/>
                    <p class="town-name">{{ $town->townName }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
