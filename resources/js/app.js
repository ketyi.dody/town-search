global.$ = require('jquery');
global.jQuery = require('jquery');
require('./bootstrap');
require('mdbootstrap');

const autocompleteInit = require('./autocompleteInit');

$(document).ready(function() {
    autocompleteInit.init();
});
