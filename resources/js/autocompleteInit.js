require('jquery-ui');
require('jquery-ui/ui/widgets/autocomplete.js');

function autocompleteInit() {
    function init() {
        $('#town-search').autocomplete({
            source: '/autocomplete',
            select: function( event, ui ) {
                var url = '/town/' + ui.item.value;
                window.location = url;
            }
        });
    }

    return {
        init,
    }
}

module.exports = autocompleteInit();
